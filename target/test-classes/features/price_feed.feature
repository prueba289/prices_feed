Feature: FX Price Feed

  Scenario Outline: Update Instrument
    Given a price feed instrument "<instrument>" with ID <id> and date "<timestamp>"
    When commissions are applied to <bid> and <ask>
    Then <ask> should be greater than <bid>
    And latest price should be displayed
    Examples:
              |  id   |  instrument    |    bid     |    ask      |           timestamp            |
              |  106  |   EUR/GBP      |   198.25   |   250.87    |    01-06-2020 12:01:01:001     |
              |  107  |   EUR/JPY      |   119.60   |   119.90    |    01-06-2020 12:01:02:002     |
              |  108  |   GBP/USD      |   1.2500   |   1.2560    |    01-06-2020 12:01:02:002     |
              |  109  |   GBP/USD      |   1.2499   |   1.2561    |    01-06-2020 12:01:02:100     |
              |  110  |   EUR/JPY      |   119.61   |   119.91    |    01-06-2020 12:01:02:002     |






