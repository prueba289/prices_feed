package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.PriceFeedServices;

public class PriceFeedSteps {

    double bid;
    double ask;
    String instrument;
    int id;
    PriceFeedServices services = new PriceFeedServices();

    @Given("a price feed instrument {string} with ID {int} and date {string}")
    public void a_price_feed_instrument_with_id_and_date(String instrument, Integer id, String string2) {
        this.instrument = instrument;
        this.id = id;
    }

    @When("commissions are applied to {double} and {double}")
    public void commissions_are_applied_to_and(Double bid, Double ask) {
      services.addCommission(bid, ask);
        this.bid = bid;
        this.ask = ask;
    }

    @Then("{double} should be greater than {double}")
    public void should_be_greater_than(Double ask, Double bid) {
        services.comparePrice(bid, ask);
    }

    @Then("latest price should be displayed")
    public void latest_price_should_be_displayed() {
        System.out.println(bid);
        System.out.println(ask);
    }
}
