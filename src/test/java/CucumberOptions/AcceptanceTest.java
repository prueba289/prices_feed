package CucumberOptions;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "StepDefinitions",
        dryRun = false,
        monochrome = true,
        plugin = {
                "pretty",
                "json:target/cucumber/authentication.json"
        }


)

public class AcceptanceTest {
}
