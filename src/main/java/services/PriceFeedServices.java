package services;

import static org.junit.Assert.assertTrue;

public class PriceFeedServices{

    private double sellPrice;
    private double buyPrice;


    //We validate each price, ensuring that bid < ask
    public void comparePrice(double bid, double ask) {

        assertTrue(bid < ask);
    }

    //We apply the commision to bid and ask
    public void addCommission(double bid, double ask){
        sellPrice = bid - 0.001;
        buyPrice = ask + 0.001;
        System.out.println(sellPrice);
        System.out.println(buyPrice);
    }
}
